package main

// DefaultTemplates enthält die HTML-Strings aller Endpunkte,
// falls keine benutzerdefinierten Templates verwendet werden
var DefaultTemplates = map[string]string{
	"ListLinks": `<html>
    <head>
        <title>Liste der Kurz-URLs</title>
        <style>
            body {
                font-family: "Avenir Next", Helvetica, Verdana, sans-serif;
            }
        </style>
    </head>
    <body>
        <h2>Vorhandene Kurz-URLs</h2>
        <table>
            <tr>
                <td><b>Kurz-URL</b></td>
                <td><b>Ziel</b></td>
            </tr>
            {{range .Entries }}<tr>
                <td>{{.ShortURL}}</td>
                <td><a href="{{.LongURL}}">{{.LongURL}}</a></td>
            </tr>{{end}}
        </table>
    </body>
    </html>`,
	"AddForm": `<html>
    <head>
        <title>Neue URL eintragen</title>
        <style>
            body {
                font-family: "Avenir Next", Helvetica, Verdana, sans-serif;
            }
        </style>
    </head>
    <body>
        <h2>Titel</h2>
        <form method="POST">
            <input name="longurl" id="longurl" type="text" placeholder="Lange URL">
            <input name="shorturl" id="shorturl" type="text" placeholder="optionale Wunsch-URL">
            <input type="submit" name="btnsubmit" id="btnsubmit">
        </form>
    </body>
    </html>`,
	"AddResult": `<html>
    <head>
        <title>Neue Kurz-URL eintragen</title>
        <style>
            body {
                font-family: "Avenir Next", Helvetica, Verdana, sans-serif;
            }
        </style>
    </head>
    <body>
        <h2>Neue Kurz-URL eintragen</h2>
        <p>Ergebnis der Neueintragung für {{.ShortURL}}</p>
        <p>{{.Result}}</p>
    </body>
    </html>`,
	"DelLink": `<html>
    <head>
        <title>Löschen einer Kurz-URL</title>
        <style>
            body {
                font-family: "Avenir Next", Helvetica, Verdana, sans-serif;
            }
        </style>
    </head>
    <body>
        <h2>Kurz-URL löschen</h2>
        <p>Ergebnis der Löschung für {{.ShortURL}}:</p>
        <p>{{.Result}}</p>
    </body>
    </html>`,
	"ShowLink": `<html>
    <head>
        <title>Anzeige Kurz-URL</title>
        <style>
            body {
                font-family: "Avenir Next", Helvetica, Verdana, sans-serif;
            }
        </style>
    </head>
    <body>
        <h2>Kurz-URL und Ziel anzeigen</h2>
        <p>Die Kurz-URL /{{.ShortURL}} hat als Ziel <a href="{{.LongURL}}">{{.LongURL}}</a></p>
    </body>
    </html>`,
}
