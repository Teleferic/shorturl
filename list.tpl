<html>
<head>
    <title>Liste der Kurz-URLs</title>
    <style>
        body {
            font-family: "Avenir Next", Helvetica, Verdana, sans-serif;
        }
    </style>
</head>
<body>
    <h2>Vorhandene Kurz-URLs</h2>
    <table>
        <tr>
            <td><b>Kurz-URL</b></td>
            <td><b>Ziel</b></td>
        </tr>
        {{range .Entries }}<tr>
            <td>{{.ShortURL}}</td>
            <td><a href="{{.LongURL}}">{{.LongURL}}</a></td>
        </tr>{{end}}
    </table>
</body>
</html>