#!/bin/bash

echo "Building Windows binaries ..."
GOOS=windows GOARCH=386 go build -o shorturl_386.exe
GOOS=windows GOARCH=amd64 go build -o shorturl_amd64.exe
echo "Building Linux binaries ..."
GOOS=linux GOARCH=386 go build -o shorturl_linux_386
GOOS=linux GOARCH=amd64 go build -o shorturl_linux_amd64
echo "Building OS X binary ..."
go build -o shorturl_osx
echo "Building Raspberry/Synology binary ..."
GOOS=linux GOARCH=arm GOARM=5 go build -o shorturl_linux_arm5
