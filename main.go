package main

import (
	"bytes"
	"crypto/subtle"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"net/http/fcgi"
	"os"
	"runtime"
	"strings"

	"github.com/BurntSushi/toml"
	"github.com/derlinkshaender/skv"
	"github.com/gorilla/mux"
	qr "github.com/skip2/go-qrcode"
	"github.com/spaolacci/murmur3"
)

// Config ist die Struktur, in der die Daten abgelegt werden,
// die beim Start des Programm aus der Konfigurationsdatei gelesen werden
type Config struct {
	User          string
	Pass          string
	LocalServer   string
	StoreFile     string
	TemplateFiles map[string]string
	Templates     map[string]*template.Template
}

var conf Config // die Variable, in der die Konfiguration liegt
var mapkeys = []string{"AddForm", "AddResult", "ShowLink", "ListLinks", "DelLink"}

const (
	// FlagRedirect bedeutet: Ziel gefunden, umleiten
	FlagRedirect = 1
	// FlagDisplay bedeutet: Ziel der Kurz-URL anzeigen
	FlagDisplay = 2
	// FlagQR bedeutet: QR-Code ausgeben
	FlagQR = 4
	// FlagInfo bedeutet: Informationen zum Ziel abrufen
	FlagInfo = 8
)

// getKeylistFromStore gibt eine Liste aller vergebenen
// Schlüssel zurück. Im Fehlerfall wird nil zurückgegeben
// der zweite Rückgabewert ist der Fehlercode
func getKeylistFromStore() ([]string, error) {
	store, err := skv.Open(conf.StoreFile)
	defer store.Close()
	if err != nil {
		return nil, err
	}
	kl, err := store.GetKeys()
	return kl, err
}

// getValueFromStore liefert einen Wert zu einem Schlüssel
// im Fehlerfall wird ein leerstring zurück geliefert
func getValueFromStore(key string) string {
	store, err := skv.Open(conf.StoreFile)
	defer store.Close()
	if err != nil {
		panic(err)
	}
	value := ""
	err = store.Get(key, &value)
	if err == nil && value != "" {
		return value
	}
	return ""
}

// putKeyToStore speichert einen wert unter einem schlüssel
// ab und liefert einen fehlercode zurück
func putKeyToStore(key, value string) error {
	store, err := skv.Open(conf.StoreFile)
	defer store.Close()
	if err != nil {
		panic(err)
	}
	if value != "" {
		return store.Put(key, value)
	}
	return skv.ErrBadValue
}

// deleteKeyFromStore entfernt den unter einem schluessel
// abgelegten wert aus dem speicher und liefert einen
// fehlercode zurück
func deleteKeyFromStore(key string) error {
	store, err := skv.Open(conf.StoreFile)
	defer store.Close()
	if err != nil {
		panic(err)
	}
	return store.Delete(key)
}

// BasicAuth wraps a handler requiring HTTP basic auth for it using the given
// username and password and the specified realm, which shouldn't contain quotes.
func BasicAuth(handler http.HandlerFunc, username, password, realm string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if !ok || subtle.ConstantTimeCompare([]byte(user), []byte(username)) != 1 || subtle.ConstantTimeCompare([]byte(pass), []byte(password)) != 1 {
			w.Header().Set("WWW-Authenticate", `Basic realm="`+realm+`"`)
			w.WriteHeader(401)
			w.Write([]byte("Unauthorised.\n"))
			return
		}
		handler(w, r)
	}
}

// encode kodiert einen Wert mit einem in "base" gelieferten alphabet als basis
func encode(nb uint64, buf *bytes.Buffer, base string) {
	l := uint64(len(base))
	if nb/l != 0 {
		encode(nb/l, buf, base)
	}
	buf.WriteByte(base[nb%l])
}

// murmurHashForURL berehcnet für einen string mit einer URL
// einen murmur-hash und liefert den zahlenwert kodiert mit
// der basis 62
func murmurHashForURL(s string, seed uint32) string {
	var buf bytes.Buffer
	h64 := murmur3.New64WithSeed(seed)
	h64.Write([]byte(s))
	v := h64.Sum64()
	encode(v, &buf, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_")
	return buf.String()
}

// parseShortURL prüft einen als shorturl übergebenen schlüssel auf zusätze
// für komfortfunktionen und liefert den bereinigten schlüssel und die
// gewünschte funktion zurück
func parseShortURL(shorturl string) (string, uint32) {
	var options uint32 = FlagRedirect
	var searchKey string
	wantDisplay := strings.HasSuffix(shorturl, ".d")
	wantQR := strings.HasSuffix(shorturl, ".q")
	wantInfo := strings.HasSuffix(shorturl, ".h")
	if wantDisplay {
		searchKey = shorturl[:len(shorturl)-2]
		options = FlagDisplay
	} else if wantQR {
		searchKey = shorturl[:len(shorturl)-2]
		options = FlagQR
	} else if wantInfo {
		searchKey = shorturl[:len(shorturl)-2]
		options = FlagInfo
	} else {
		searchKey = shorturl
		options = FlagRedirect
	}
	return searchKey, options
}

// isValidShortURL prüft, ob die übergebene kurz-URL gültig ist,
// d.h. keine unzulässigen zeichen enthält und nicht bereits existiert
func isValidShortURL(shorturl string) bool {
	var invalid = " {}|\\^[]`;/?:@&=+$,\"'"
	valid := !strings.ContainsAny(shorturl, invalid)
	exists := getValueFromStore(shorturl) != ""
	return valid && !exists
}

// addLinkView ist die View für das hinzufügen einer neuen kurz-URL
// abhängig von der HTTP-Methode wird entweder das Formular ausgegeben
// oder verarbeitet
func addLinkView(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		conf.Templates["AddForm"].Execute(w, nil)
	} else if r.Method == "POST" {
		type AddData struct {
			ShortURL string
			LongURL  string
			Result   string
			OK       bool
		}
		var data AddData
		validShort := true
		r.ParseForm()
		data.LongURL = strings.Join(r.Form["longurl"], "")
		data.ShortURL = strings.Join(r.Form["shorturl"], "")
		if data.ShortURL == "" {
			data.ShortURL = murmurHashForURL(data.LongURL, 0x01)
			validShort = true
		} else {
			validShort = isValidShortURL(data.ShortURL)
		}
		if validShort {
			err := putKeyToStore(data.ShortURL, data.LongURL)
			if err == nil {
				data.Result = "Kurz-URL erfolgreich eingetragen"
				data.OK = true
			} else {
				data.Result = err.Error()
				data.OK = false
			}
		} else {
			data.Result = "Die angegebene Kurz-URL ist ungültig"
			data.OK = false
		}
		conf.Templates["AddResult"].Execute(w, data)
	}
}

func delLinkView(w http.ResponseWriter, r *http.Request) {
	type AddData struct {
		ShortURL string
		LongURL  string
		Result   string
		OK       bool
	}
	var data AddData
	vars := mux.Vars(r)
	data.ShortURL = vars["key"]
	data.LongURL = "" // we do not need the longURL, just to be safe in template
	err := deleteKeyFromStore(data.ShortURL)
	if err == skv.ErrNotFound {
		data.Result = "Schlüssel existiert nicht."
		data.OK = false
	} else if err != nil {
		data.Result = " " + err.Error()
		data.OK = false
	} else {
		data.Result = "Schlüssel wurde gelöscht."
		data.OK = true
	}
	conf.Templates["DelLink"].Execute(w, data)
}

func listLinkView(w http.ResponseWriter, r *http.Request) {
	type entry struct {
		ShortURL string
		LongURL  string
	}
	type pgdata struct {
		Entries []entry
	}
	var err error
	var data pgdata
	format := "html" // the default format, may as well be specified explicitly
	keys, ok := r.URL.Query()["format"]
	if ok {
		format = strings.ToLower(strings.Join(keys, ""))
	}

	kl, err := getKeylistFromStore()
	if err == nil {
		for _, k := range kl {
			destination := getValueFromStore(k)
			if destination == "" {
				destination = fmt.Sprintf("Fehler beim Lesen von Schlüssel %s", k)
			}
			data.Entries = append(data.Entries, entry{ShortURL: k, LongURL: destination})
		}
	} else {
		http.Error(w, "Error retrieving key list", 500)
	}
	switch format {
	case "html":
		conf.Templates["ListLinks"].Execute(w, data)
	case "csv":
		s := ""
		for _, r := range data.Entries {
			s += r.ShortURL + ", " + r.LongURL + "\n"
		}
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		io.WriteString(w, s)
	case "json":
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		wr := json.NewEncoder(w)
		wr.Encode(data)
	case "xml":
		w.Header().Set("Content-Type", "application/xml; charset=utf-8")
		wr := xml.NewEncoder(w)
		wr.Encode(data)
	}
}

func expanderView(w http.ResponseWriter, r *http.Request) {
	var msg string
	vars := mux.Vars(r)
	headers := w.Header()
	shorturl := vars["path"]
	searchKey, options := parseShortURL(shorturl)
	destination := getValueFromStore(searchKey)
	if destination == "" {
		http.Error(w, fmt.Sprintf("Kurz-Code %s nicht gefunden", searchKey), 404)
	} else {
		// we got a destination
		switch options {
		case FlagRedirect:
			http.Redirect(w, r, destination, 307)
		case FlagDisplay:
			type AddData struct {
				ShortURL string
				LongURL  string
			}
			var data AddData
			data.ShortURL = searchKey
			data.LongURL = destination
			conf.Templates["ShowLink"].Execute(w, data)
		case FlagQR:
			q, err := qr.New(destination, qr.Medium) //QR-Code enthält das Ziel!
			if err != nil {
				msg = "Error during creation of QR code" + err.Error()
				headers.Add("Content-Type", "text/html")
				io.WriteString(w, "<html><head></head><body><p>"+msg+"</p></body></html>")
			}
			w.Header().Set("Content-Type", "image/png") // <-- set the content-type header
			q.Write(400, w)
		case FlagInfo:
			res, err := http.Head(destination)
			if err != nil {
				panic(err)
			}
			headerLines := []string{}
			for k, v := range res.Header {
				headerLines = append(headerLines, k+":"+strings.Join(v, "\n"))
			}
			headers.Add("Content-Type", "text/html; charset=utf-8")
			io.WriteString(w, "<html><head></head><body style=\"font-family: Verdana,Helvetica,sans-serif;\"><h2>HTTP HEAD</h2><p>Anzeige für: "+destination+"</p><p>"+strings.Join(headerLines, "<br>\n")+"</p></body></html>")
		default:
			// we did not find anything
			msg = "No entry for short URL " + shorturl
			headers.Add("Content-Type", "text/html; charset=utf-8")
			io.WriteString(w, "<html><head></head><body><p>"+msg+"</p></body></html>")
		}
	}
}

// alle vorhandenen CPUs nutzen
func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

// loadTemplates created html template instances.
// either by loading from a specified file or from the default template string
func loadTemplates() error {
	var err error
	conf.Templates = make(map[string]*template.Template)
	for _, k := range mapkeys {
		conf.Templates[k] = template.New(k)
		if val, ok := conf.TemplateFiles[k]; ok {
			if conf.LocalServer != "" {
				log.Println(fmt.Sprintf("Loading %s from file %s", k, val))
			}
			conf.Templates[k], err = conf.Templates[k].ParseFiles(val)
		} else {
			if conf.LocalServer != "" {
				log.Println(fmt.Sprintf("Using default template for %s", k))
			}
			conf.Templates[k], err = conf.Templates[k].Parse(DefaultTemplates[k])
		}
		if err != nil {
			return err
		}
	}
	return err
}

func main() {
	var err error
	if _, err = toml.DecodeFile("./shorturl.toml", &conf); err != nil {
		log.Fatal(err.Error())
		os.Exit(1)
	} else {
		if err = loadTemplates(); err != nil {
			log.Fatal(err.Error())
			os.Exit(1)
		}
	}

	/*
		Folgende Routen werden benötigt:
		/admin/add zum Anlegen einer neuen Kurz-URL
		/admin/del/key zum Entfernen einer eingetragenen Kurz-URL
		/admin/list zum Auflisten aller vorhandenen Kurz-URLs und ihrer Ziele
		/<KURZCODE> zum Aufrufen einer Kurz-URL
	*/
	r := mux.NewRouter()
	r.HandleFunc("/admin/add", BasicAuth(addLinkView, conf.User, conf.Pass, "Shorty URL Service"))
	r.HandleFunc("/admin/del/{key}", BasicAuth(delLinkView, conf.User, conf.Pass, "Shorty URL Service"))
	r.HandleFunc("/admin/list", listLinkView)
	r.HandleFunc("/{path}", expanderView)
	if conf.LocalServer != "" {
		log.Println(fmt.Sprintf("Running with local server at %s", conf.LocalServer))
		err = http.ListenAndServe(conf.LocalServer, r)
	} else { // Run as FCGI via standard I/O
		err = fcgi.Serve(nil, r)
	}
	if err != nil {
		log.Fatal(err)
	}
}
